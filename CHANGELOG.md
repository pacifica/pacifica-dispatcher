# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.3] - 2019-05-17
### Added
- Download of data from the cartd service
  - Using cloud events
- Upload of data to the ingest service
- Services to catch cloud events
- Local variants of download and upload
- Router to dispatch events to different receivers
- ReadtheDocs supported Sphinx docs

### Changed
